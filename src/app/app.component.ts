import { Component } from '@angular/core';
import { AuthorFormDialogComponent } from './form-dialogs/author-form-dialog/author-form-dialog.component';
import { BookFormDialogComponent } from './form-dialogs/book-form-dialog/book-form-dialog.component';
import { LanguageFormDialogComponent } from './form-dialogs/language-form-dialog/language-form-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'bookstore';
  AuthorFormDialogType = AuthorFormDialogComponent;
  BookFormDialogComponent = BookFormDialogComponent;
  LanguageFormDialogComponent = LanguageFormDialogComponent;
}
