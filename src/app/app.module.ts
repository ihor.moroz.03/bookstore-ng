import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';

import { AppComponent } from './app.component';
import { DataTableComponent } from './data-table/data-table.component';
import { BookFormDialogComponent } from './form-dialogs/book-form-dialog/book-form-dialog.component';
import { AuthorFormDialogComponent } from './form-dialogs/author-form-dialog/author-form-dialog.component';
import { LanguageFormDialogComponent } from './form-dialogs/language-form-dialog/language-form-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    DataTableComponent,
    BookFormDialogComponent,
    AuthorFormDialogComponent,
    LanguageFormDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    HttpClientModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
