import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../data-table/data-table.component';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-author-form-dialog',
  templateUrl: './author-form-dialog.component.html',
  styleUrls: ['./author-form-dialog.component.scss'],
})
export class AuthorFormDialogComponent {
  readonly apiRoute: string = 'https://localhost:7125/authors';

  form: FormGroup = new FormGroup({
    id: new FormControl(this.data.item.id ?? 0),
    name: new FormControl(this.data.item.name),
  });

  constructor(
    public dialogReg: MatDialogRef<AuthorFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient
  ) {}

  add() {
    this.http.post(this.apiRoute, this.form.getRawValue()).subscribe({
      next: (response) => {
        console.log(response);
      },
    });
  }

  edit() {
    this.http
      .put(this.apiRoute + `/${this.data.item.id}`, this.form.getRawValue())
      .subscribe({
        next: (response) => {
          console.log(response);
        },
      });
  }
}
