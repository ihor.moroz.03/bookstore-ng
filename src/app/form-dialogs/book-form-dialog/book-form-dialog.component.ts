import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../data-table/data-table.component';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-book-form-dialog',
  templateUrl: './book-form-dialog.component.html',
  styleUrls: ['./book-form-dialog.component.scss'],
})
export class BookFormDialogComponent {
  readonly apiRoute: string = 'https://localhost:7125/books';
  authors: any[] = [];
  languages: any[] = [];

  form: FormGroup = new FormGroup({
    id: new FormControl(this.data.item.id ?? 0),
    title: new FormControl(this.data.item.title),
    publicationYear: new FormControl(this.data.item.publicationYear),
    author: new FormControl(),
    language: new FormControl(),
  });

  constructor(
    public dialogReg: MatDialogRef<BookFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient
  ) {
    this.http.get<any[]>('https://localhost:7125/authors').subscribe({
      next: (authors) => {
        this.authors = authors;
      },
    });

    this.http.get<any[]>('https://localhost:7125/languages').subscribe({
      next: (languages) => {
        this.languages = languages;
      },
    });
  }

  add() {
    debugger;
    this.http.post(this.apiRoute, this.form.getRawValue()).subscribe({
      next: (response) => {
        console.log(response);
      },
    });
  }

  edit() {
    this.http
      .put(this.apiRoute + `/${this.data.item.id}`, this.form.getRawValue())
      .subscribe({
        next: (response) => {
          console.log(response);
        },
      });
  }
}
