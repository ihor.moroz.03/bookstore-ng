import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../data-table/data-table.component';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-language-form-dialog',
  templateUrl: './language-form-dialog.component.html',
  styleUrls: ['./language-form-dialog.component.scss'],
})
export class LanguageFormDialogComponent {
  readonly apiRoute: string = 'https://localhost:7125/languages';

  form: FormGroup = new FormGroup({
    id: new FormControl(this.data.item.id ?? 0),
    code: new FormControl(this.data.item.code),
    name: new FormControl(this.data.item.name),
  });

  constructor(
    public dialogReg: MatDialogRef<LanguageFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient
  ) {}

  add() {
    this.http.post(this.apiRoute, this.form.getRawValue()).subscribe({
      next: (response) => {
        console.log(response);
      },
    });
  }

  edit() {
    this.http
      .put(this.apiRoute + `/${this.data.item.id}`, this.form.getRawValue())
      .subscribe({
        next: (response) => {
          console.log(response);
        },
      });
  }
}
