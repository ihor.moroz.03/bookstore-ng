import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageFormDialogComponent } from './language-form-dialog.component';

describe('LanguageFormDialogComponent', () => {
  let component: LanguageFormDialogComponent;
  let fixture: ComponentFixture<LanguageFormDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageFormDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LanguageFormDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
